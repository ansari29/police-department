class Traffic:

  def __init__(self,owner,vehicle,number,date,reciept):
    self.owner = owner
    self.vehicle = vehicle
    self.number = number
    self.date = date
    self.reciept = reciept

class Data:

  def __init__(self,challan_per_day,cost,days):
    self.challan = challan_per_day
    self.cost = cost
    self.days = days

class Challan(Data,Traffic):
  def __init__(self,owner,vehicle,number,date,reciept,challan_per_day,cost,days,discount):
    Traffic.__init__(self,owner,vehicle,number,date,reciept)
    Data.__init__(self,challan_per_day,cost,days) 
    self.discount = discount 

  def bill(self):
    bill = self.challan * self.cost * self.days
    disc = bill * (self.discount/100)
    return bill - disc

  def display(self):
    return f'Reciept number: {self.reciept} Reciept generation date: {self.date} \nOwner name: {self.owner}\nVehicle number: {self.number}\nVehicle name: {self.vehicle}\nTotal Challan after discount: {self.bill()}'

person1 = Challan('Ansari','Activa',3469,'15-12-2022','0157',12,120,30,20)
print(person1.display())   